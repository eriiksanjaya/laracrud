<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_siswa extends Model
{
    public $prefix              = 'siswa_';
    protected $table            = 'master_siswa';
    protected $primaryKey       = 'siswa_id';
    protected $guarded          = [];
    public $timestamps          = false;

    const CREATED_AT = 'siswa_created_at';
    const UPDATED_AT = 'siswa_updated_at';
}