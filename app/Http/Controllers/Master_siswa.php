<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\M_siswa as model;

class Master_siswa extends Controller
{
    private $fullurl 	= '';
	private $initurl 	= 'siswa';
	private $prefix 	= 'siswa';
	private $_title 	= 'Siswa';

    public function index() {
        $data['title']      = 'Data Siswa';
        $data['breadcrumb'] = [ ['Home', route('dashboard.index')], ['Siswa', route('siswa.index')] ];
        $data['getdata']    = model::all();
        return view('siswa.siswa', ['data' => $data]);
    }

    public function show($id) {
        $getdata = \DB::table('master_siswa')->where('siswa_id', $id)->get();
        $getdata = \DB::table('master_siswa')->get();
        dd($getdata);
    }

    public function create() {
        $data['title']      = 'Form Tambah';
        $data['breadcrumb'] = [ ['Home', route('dashboard.index')], ['Siswa', route('siswa.index')], ['Tambah', route('siswa.create')] ];
        $data['action']     = 'add';
        $data['redirect']   = 'siswa.index';
        $data['route']      = 'siswa.store';

        return view('siswa.siswa_form', ['data' => $data]);
    }

    public function store(Request $request) {

        $res['status']     = 422;
        $res['message']    = "Data {$this->_title} gagal ditambah.";

        $data['siswa_nama']         = $request['nama'];
        $data['siswa_jk']           = $request['kelamin'];
        $data['siswa_agama']        = $request['agama'];
        $data['siswa_alamat']       = $request['alamat'];
        $data['siswa_created_at']   = date('Y-m-d H:i:s');
        
        $save = model::create($data);

        if ($save->wasRecentlyCreated) {
            $res['status']     = 200;
            $name              = $data['siswa_nama'];
            $res['message']    = "Data {$this->_title} <strong>{$name}</strong> berhasil ditambah.";
        }

        return response()->json($res);
    }

    public function edit($id) {
        $data['status']     = 404;
        $data['message']    = "Data {$this->_title} tidak ditemukan.";

        $data['title']      = 'Form Edit';
        $data['breadcrumb'] = [ ['Home', route('dashboard.index')], ['Siswa', route('siswa.index')], ['Edit', route('siswa.edit', $id)] ];
        $data['action']     = 'edit';
        $data['redirect']   = 'siswa.index';
        $data['route']      = 'siswa.update';
        $data['id']         = $id;
        // $data['getdata']    = model::where('siswa_id', $id)->get();
        $data['getdata']    = model::find($id);
        $data['getdata']    = model::where('siswa_id', $id)->first();

        if($data['getdata'] != null) {
            $data['status']     = 200;
            $data['message']    = "Data {$this->_title} ditemukan.";
            return view('siswa.siswa_form', ['data' => $data]);
        } else {
            return view('error.404', ['data' => $data]);
        }

    }

    public function update(Request $request, $id) {

        $res['status']     = 422;
        $res['message']    = "Data {$this->_title} gagal diupdate.";

        $data['siswa_nama']         = $request['nama'];
        $data['siswa_jk']           = $request['kelamin'];
        $data['siswa_agama']        = $request['agama'];
        $data['siswa_alamat']       = $request['alamat'];
        $data['siswa_updated_at']   = date('Y-m-d H:i:s');

        $update = model::where('siswa_id', $id)->update($data);

        if ($update) {
            $res['status']     = 200;
            $name              = $data['siswa_nama'];
            $res['message']    = "Data {$this->_title} {$name} berhasil diupdate.";
        }

        return response()->json($res);
    }

    public function delete($id) {

        $res['status']     = 422;
        $res['message']    = "Data {$this->_title} gagal dihapus.";

        $delete = model::find($id)->delete($id); //return true if success

        if ($delete) {
            $res['status']     = 200;
            $res['message']    = "Data {$this->_title} berhasil dihapus.";
        }

        return response()->json($res);
    }
}
