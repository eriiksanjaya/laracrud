var mApp = function() {

    var _pjax = function() {
        var pjax = new Pjax({
            cacheBust: false,
            elements: "a.pjax",
            selectors: ['title', 'meta[name=description]', '.m-subheader', '.m-content'],
        })

        $('html').on('click', '.btn-edit', function(e) {
            e.preventDefault();
            var link_pjax = e.originalEvent.srcElement.parentElement.dataset.url;
            if (link_pjax) {
                $('#link-pjax').attr('href', link_pjax)[0].click();
            }
        })

    }

    var _action_status = function() {
        $("body").on('click', '.action_status', function(event) {
            event.preventDefault();

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "3000",
                "hideDuration": "3000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            var url         = $(this).attr('mydata-url');
            var status      = $(this).attr('mydata-status');
            var id          = $(this).attr('mydata-id');

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {id: id, status: status},
            })
            .done(function(res) {
                if (res.status == 200) {
                    toastr.success(res.message, "Success");
                } else {
                    toastr.error(res.message, "Error");
                }
                
                if ( $('#reloadTable').length ) {
                    $('#reloadTable')[0].click();
                }
            })
            .fail(function(res) {
                toastr.error(res.message, "Error");
            })
            .always(function() {
            });
        });
    }

    var _action_delete = function() {
        $("body").on('click', '#action_delete', function() {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "3000",
                "hideDuration": "3000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            var url         = $(this).attr('mydata-url');
            var id          = $(this).attr('mydata-id');
            var name        = $(this).attr('mydata-name');

            var text        = 'Data yang telah dihapus <strong>tidak dapat dikembalikan</strong>. apakah anda yakin ingin menghapus data ini?';

            Swal.fire({
                title: 'Yakin Hapus Data',
                html: text,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'json',
                        data: {id: id, name: name},
                    })
                    .done(function(res) {
                        if (res.status == 200) {
                            toastr.success(res.message, "Success");
                        } else {
                            toastr.error(res.message, "Error");
                        }
                        if ( $('#reloadTable').length ) {
                            $('#reloadTable')[0].click();
                        }
                        
                    })
                    .fail(function(res) {
                        toastr.error(res.message, "Error");
                    })
                    .always(function() {
                    });          

                } else if (result.dismiss === Swal.DismissReason.cancel) {
                }
            })

            
        });
    }

    var handleDatePicker = function() {
        $('.app-datepicker').datepicker({
            format: 'dd-mm-yyyy',
        });
    }

    var handleDropzone = function() {
        Dropzone.autoDiscover = false;
        var dropZone = new Dropzone("#m-dropzone-one");
        Dropzone.options.mDropzoneOne = {
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: 1,
            maxFilesize: 2, // MB
            addRemoveLinks: false,
            completemultiple: true,
            accept: function(file) {
                console.log(file);
            },
        };
    }

    var handleReinitSelect2 = function(elem)
    {
        var url           = $(this).data('url');
        var placeholder   = $(this).data('placeholder') != undefined ? $(this).data('placeholder') : 'silakan cari / pilih...';
        var minimum       = $(this).data('min-length') != undefined ? $(this).data('min-length') : 0;                                       

        $(elem).select2({
            placeholder: placeholder,
            allowClear: true,
            ajax: {
                url: url,
                data: function (params) {
                    var query = {
                        q: params.term,
                        // tipe: tipe
                    }
                    return query;
                },
                processResults: function (data, page) {
                    return {
                        results: data.items
                    };
                },
            },
            language: {
                noResults: function(){
                    return 'data tidak ditemukan!';
                },
                inputTooShort: function () {
                    return placeholder;
                }
            },
            minimumInputLength: minimum
        });
    }

    var handleSelect2 = function() {

        if ( $('.appselect2-basic').length > 0 ) {
            // $(".appselect2-basic").val('').trigger('change');
            $('.appselect2-basic').each(function(index, el) {
                $(this).select2({
                    placeholder: "silakan pilih...",
                    minimumResultsForSearch: Infinity
                });
            });
        }

        if ( $('.appselect2-basic-search').length > 0 ) {
            // $(".appselect2-basic-search").val('').trigger('change');
            $('.appselect2-basic-search').each(function(index, el) {
                $(this).select2({
                    placeholder: "silakan cari / pilih...",
                });
            });
        }

        if ( $('.appselect2-me').length > 0 ) {
            // $(".appselect2-me").val('').trigger('change');
            $('.appselect2-me').each(function(index, el) {
                var url           = $(this).data('url');
                var placeholder   = $(this).data('placeholder') != undefined ? $(this).data('placeholder') : 'silakan cari / pilih...';
                var minimum       = $(this).data('min-length') != undefined ? $(this).data('min-length') : 0;   
                
                $(this).select2({
                    placeholder: placeholder,
                    allowClear: false,
                    ajax: {
                        url: url,
                        data: function (params) {
                            var query = {
                                q: params.term,
                            }
                            return query;
                        },
                        processResults: function (data, page) {
                            return {
                                results: data.items
                            };
                        },
                    },
                    language: {
                        noResults: function(){
                            return 'data tidak ditemukan!';
                        },
                        inputTooShort: function () {
                            return placeholder;
                        }
                    },
                    minimumInputLength: minimum,
                    minimumResultsForSearch: Infinity,
                });

            });
        }

        if ( $('.appselect2-advance').length > 0 ) {
            // $(".appselect2-advance").val('').trigger('change');
            $('.appselect2-advance').each(function(index, el) {
                var url           = $(this).data('url');
                // var tipe          = $(this).data('tipe') != undefined ? $(this).data('tipe') : 'desa';
                var placeholder   = $(this).data('placeholder') != undefined ? $(this).data('placeholder') : 'silakan cari / pilih...';
                var minimum       = $(this).data('min-length') != undefined ? $(this).data('min-length') : 0;   
                
                $(this).select2({
                    placeholder: placeholder,
                    allowClear: false,
                    ajax: {
                        url: url,
                        data: function (params) {
                            var query = {
                                q: params.term,
                                // tipe: tipe
                            }
                            return query;
                        },
                        processResults: function (data, page) {
                            return {
                                results: data.items
                            };
                        },
                    },
                    language: {
                        noResults: function(){
                            return 'data tidak ditemukan!';
                        },
                        inputTooShort: function () {
                            return placeholder;
                        }
                    },
                    minimumInputLength: minimum
                });

            });
        }
    }

    var initTable1 = function(container, url, header, order, unsortable, base_url) {
        // begin first table
        var ajaxParams = {};

        var table = $(container).DataTable({
            responsive: true,
            //== Pagination settings
            dom: `<'row'<'col-sm-12'tr>>
            <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            // read more: https://datatables.net/examples/basic_init/dom.html

            lengthMenu: [5, 10, 25, 50],

            pageLength: 10,

            language: {
                'lengthMenu': 'Display _MENU_',
            },
            stateSave: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            order: order,
            aoColumnDefs: [ { "bSortable": false, "aTargets": unsortable } ],
            ajax: {
                url: url,
                type: 'POST',
                dataType: 'json',
                data: function(data, con){
                    $.each(ajaxParams, function(key, value) {
                            data[key] = value;
                    });
                },
                "dataSrc": function(res) { // Manipulate the data returned from the server
                    if (res.status == 401) {
                        window.location.href = base_url+'account/logout';
                    } else {
                        return res.data
                    }
                }
            },
            /*drawCallback: function (res) { 
                console.log(res)
            },*/
            aoColumns: header,
        });

        function setAjaxParam(name, value) {
            ajaxParams[name] = value;
        }

        $('#m_search').on('click', function(e) {
            e.preventDefault();
            $('input.m-input').each(function() {
                setAjaxParam('action', 'filter');
                setAjaxParam($(this).attr("name"), $(this).val());
            });

            $('select.m-input').each(function() {
                setAjaxParam('action', 'filter');
                setAjaxParam($(this).attr("name"), $(this).val());
            });

            table.ajax.reload();  //just reload table
        });

        $('#m_reset').on('click', function(e) {
            e.preventDefault();
            $('.m-input').each(function() {
                $(this).val('');
                setAjaxParam('action', 'filter');
                setAjaxParam($(this).attr("name"), $(this).val());
                $(".appselect2-basic").val('').trigger('change');
                $(".appselect2-basic-search").val('').trigger('change');
                $(".appselect2-me").val('').trigger('change');
                $(".appselect2-advance").val('').trigger('change');
            });
            
            table.ajax.reload();  //just reload table
        });

        $('#reloadTable').on('click', function(e) {
            e.preventDefault();
            table.ajax.reload();  //just reload table
        });

        $('#m_datepicker').datepicker({
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
        });

    };

    var handleValidation = function(_form, _rules, _messages) {

        $("body").on('click', '#submit', function() {

            var form        = $(_form);
            var warning     = $('.form-action');

            var action  = form.attr('action');
            var confirm = (typeof form.attr('data-confirm') === "undefined") ? 0 : form.attr('data-confirm');

            title = (typeof title === "undefined") ? "Anda yakin?" : title;
            text  = (typeof text === "undefined") ? "Periksa kembali data sebelum melanjutkan. Lanjutkan proses?" : text;
            
            var data    = '';

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "3000",
                "hideDuration": "3000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            form.validate({
                errorElement: 'label', // default input error message container
                errorClass: 'label', // default input error message class
                focusInvalid : false, // do not focus the last invalid input
                ignore: false, //validate all fields including form hidden input
                messages : _messages,
                rules : _rules,
                invalidHandler : function(event, validator) { // display error alert on submit
                    warning.slideDown('slow');
                    warning.find('span.alert-message').first().html('Data yang anda isikan belum valid, silakan periksa kembali.');                
                    // mUtil.scrollTop();
                },
                errorPlacement: function(error, element) {
                    if (element.is(':checkbox')) {
                        // error.insertAfter(element.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline"));
                    } else if (element.is(':radio')) {
                        // error.insertAfter(element.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline"));
                    } else if ( element.hasClass('select2') ) {
                        // error.css('margin-top', '8px').appendTo(element.parent());
                    } else if ( element.hasClass('summernote') || element.hasClass('summernoteme') ) {
                        // var a = element.parents('.form-group').find('.help-block').first().html(error.html());
                        // a.attr('id', error.attr('id'));
                        // a.attr('class', error.attr('class'));                    
                        // error.css('margin-top', '8px').appendTo(element.parent());                 
                    } else {
                        // error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },
                highlight: function(element) { // hightlight error inputs
                    // $(element).closest('.form-group').addClass('has-danger'); // set error class to the control group
                },

                unhighlight: function(element) { // revert the change done by hightlight
                    // $(element)closest('.form-group').removeClass('has-danger'); // set error class to the control group
                },

                success: function(element) {
                    // element.closest('.form-group').removeClass('has-danger'); // set success class to the control group
                },
                submitHandler: function(form) {

                    var options = { 
                        dataType: 'json',
                        success: callback_form,
                        error: callback_error
                    }; 

                    if ( confirm == 1 ) {
                        Swal.fire({
                        title: title,
                        text: text,
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Ya',
                        cancelButtonText: 'Tidak'
                        }).then((result) => {
                        if (result.value) {
                            $(form).ajaxSubmit(options);
                        } else if (result.dismiss === Swal.DismissReason.cancel) {
                            // mApp.unblock('.m-portlet');
                        }
                        })

                    } else {
                        $(form).ajaxSubmit(options);
                    }
                }
            });

            function callback_form(res, statusText, xhr, $form)
            {
                
                // code_ok                      = 200;
                // code_unauthorized            = 401; // key is not valid
                // code_forbidden               = 403;
                // code_notfound                = 404; 
                // code_unprocessable_entity    = 422; // data dipahami, tapi tidak valid

                if ( res.status == 200 ) {
                    // mApp.unblock('.m-portlet');
                    warning.hide();
                    toastr.success(res.message, "Success");

                    if($('.myredirect').length)
                    {
                        if ( res.redirect != undefined && res.redirect != "" ) {                        
                            $('.myredirect').attr('href', res.redirect);
                        } 
                        $('.myredirect')[0].click();
                    }

                } else {
                    // mApp.unblock('.m-portlet');
                    $(".form-action").slideDown('slow');
                    // warning.slideDown('slow');
                    warning.find('span.alert-message').first().html(res.message);
                    // mUtil.scrollTop();
                }

                // App.stopPageLoading();
            }

            function callback_error(){
                // mApp.unblock('.m-portlet');
                // toastr.success(res.message, "Success");
                // toastr.warning(res.message, "Warning");
                toastr.error('Terjadi kesalahan pada sistem, hubungi System Admin!', "Error");
            }
        }); // end click submit
    }
    
    var handleFormAction = function() {
        $("body").on('click', '.form-action', function() {
            $(this).slideUp('slow');
        });  
    }

    return {
        
        init: function(options, container, url, header, order, unsortable, base_url) {
            mApp.pjax();
            // mApp.initSelect2();
            // mApp.initDatePicker();
            // mApp.initDropzone();
            mApp.action_delete();

        },

        formValidation: function() {
            handleValidation('#myForm', {}, {});
            handleFormAction();
            $(".form-action").slideUp();
        },

        action_delete: function() {
            _action_delete();
        },

        pjax: function() {
            _pjax();
        },

        initTable1: function(container, url, header, order, unsortable, base_url) {
            initTable1(container, url, header, order, unsortable, base_url);
            _action_delete();
            _action_status();
        },

        initSelect2 : function() {
            handleSelect2();
        },

        initDatePicker : function() {
            handleDatePicker();
        },

        initDropzone : function() {
            handleDropzone();
        },

        reinitSelect2 : function(elem) {
            handleReinitSelect2(elem);
        }

    };
}();