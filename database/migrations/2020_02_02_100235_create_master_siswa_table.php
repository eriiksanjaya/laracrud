<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterSiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('master_siswa');

        Schema::create('master_siswa', function (Blueprint $table) {
            $table->bigIncrements('siswa_id');
            $table->string('siswa_nama', 50)->nullable();
            $table->enum('siswa_jk', ['L', 'P'])->nullable();
            $table->enum('siswa_agama', ['ISLAM', 'PROTESTAN', 'KATOLIK', 'HINDU', 'BUDHA'])->nullable();
            $table->text('siswa_alamat')->nullable();
            $table->datetime('siswa_created_at', 0)->nullable();
            $table->datetime('siswa_updated_at', 0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_siswa');
    }
}
