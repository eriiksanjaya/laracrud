@extends('layouts/app')

@section('title')
    {{ $data['title'] }}
@endsection

@section('breadcrumb')
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        @foreach ($data['breadcrumb'] as $key => $item)
            <li class="breadcrumb-item"><a class="pjax" href="{{ $item[1] }}">{{ $item[0] }}</a></li>
        @endforeach
    </ol>
  </nav>
@endsection

@section('content')
    <h1 class="display">{{ $data['message'] }}</h1>
@endsection