@extends('layouts/app')
@section('title')
    {{ $data['title'] }}
@endsection
@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb rounded">
            @foreach ($data['breadcrumb'] as $key => $item)
                <li class="breadcrumb-item"><a class="pjax" href="{{ $item[1] }}">{{ $item[0] }}</a></li>
            @endforeach
        </ol>
    </nav>
@endsection
@section('content')
    <div class="card mb-3">
        <div class="card-header shadow bg-white rounded">
            <div class="col-md-6 float-left">
                <h5 class="font-weight-light">
                    {{$data['title']}}
                </h5>
            </div>
            <div class="col-md-6 float-right">
                <div class="float-right">
                    <a class="btn btn-sm btn-outline-info pjax" tabindex="-1" role="button" id="reloadTable" href="{{ route('siswa.index') }}">Reload</a>
                    <a class="btn btn-sm btn-outline-primary btn-primary pjax" tabindex="-1" role="button" href="{{ route('siswa.create') }}">Tambah</a>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive shadow bg-white rounded">
        <table class="table table-borderedx">
            <thead class="thead-light">
                <tr>
                    <th class="text-center">No</th>
                    <th>ID</th>
                    <th>Nama</th>
                    <th class="text-center">Kelamin</th>
                    <th class="text-center">Agama</th>
                    <th>Alamat</th>
                    <th>Created</th>
                    <th>Updated</th>
                    <th width=180 class="text-center">Action</th>
                </tr>
            </thead>

            @foreach ($data['getdata'] as $key => $value)
                <tr>
                    <td class="text-center">{{ $key+1 }}</td>
                    <td>{{ $value->siswa_id }}</td>
                    <td>{{ $value->siswa_nama }}</td>
                    <td class="text-center">{{ $value->siswa_jk }}</td>
                    <td class="text-center">{{ $value->siswa_agama }}</td>
                    <td>{{ $value->siswa_alamat }}</td>
                    <td>{{ $value->siswa_created_at }}</td>
                    <td>{{ $value->siswa_updated_at }}</td>
                    <td class="text-center">
                        <a class="btn btn-sm btn-outline-warning pjax" href="{{ route('siswa.edit', $value->siswa_id) }}">Edit</a>
                        {{-- <a class="btn btn-danger" href="{{ route('siswa.destroy', $value->siswa_id) }}">Delete</a> --}}
                        {{-- <a class="btn btn-danger action_delete"  href="/siswa/{{$value->siswa_id}}/delete">Delete</a> --}}
                        <a class="btn btn-sm btn-outline-danger action_delete" id="action_delete" href="javascript:;" mydata-url="{{ route('siswa.destroy', $value->siswa_id) }}" mydata="{{$value->siswa_id}}" mydata="{{$value->siswa_nama}}">Delete</a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection