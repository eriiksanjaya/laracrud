@extends('layouts/app')

@section('title')
    {{ $data['title'] }}
@endsection

@section('breadcrumb')
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        @foreach ($data['breadcrumb'] as $key => $item)
            <li class="breadcrumb-item"><a class="pjax" href="{{ $item[1] }}">{{ $item[0] }}</a></li>
        @endforeach
    </ol>
  </nav>
@endsection

@section('content')

    <div class="card shadow bg-white rounded">
        <div class="card-header">
            <h5 class="font-weight-light">
                {{ $data['title'] }}
            </h5>
        </div>
        <div class="card-body">
            <div class="alert alert-info form-action">
                <span class="alert-message"></span>
            </div>
            
            <form action="{{ isset($data['id']) ? route($data['route'], $data['id']) : route($data['route']) }}" method="POST" id="myForm" data-confirm="1">
                @csrf
                @if (isset($data['id']))
                    {{ method_field('PUT') }}
                @endif
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" name="nama" value="{{ @$data['getdata']->siswa_nama }}" required>
                </div>

                <div class="form-group">
                    <label>Jenis Kelamin</label>
                    <select class="form-control" name="kelamin" required>
                        <option value="L" @if (@$data['getdata']->siswa_jk == 'L') ? selected @endif>Laki-Laki</option>
                        <option value="P" @if (@$data['getdata']->siswa_jk == 'P') ? selected @endif>Perempuan</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Agama</label>
                    <select class="form-control" name="agama" required>
                        <option value="ISLAM" @if (@$data['getdata']->siswa_agama == 'ISLAM') ? selected @endif>ISLAM</option>
                        <option value="PROTESTAN" @if (@$data['getdata']->siswa_agama == 'PROTESTAN') ? selected @endif>PROTESTAN</option>
                        <option value="KATOLIK" @if (@$data['getdata']->siswa_agama == 'KATOLIK') ? selected @endif>KATOLIK</option>
                        <option value="HINDU" @if (@$data['getdata']->siswa_agama == 'HINDU') ? selected @endif>HINDU</option>
                        <option value="BUDHA" @if (@$data['getdata']->siswa_agama == 'BUDHA') ? selected @endif>BUDHA</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Alamat</label>
                    <textarea class="form-control" name="alamat" required> {{ @$data['getdata']->siswa_alamat }} </textarea>
                </div>

                <div class="form-group">
                    <a class="btn btn-outline-secondary pjax" href="{{ route($data['redirect']) }}"> Kembali </a>
                    <input type="submit" id="submit" class="btn btn-outline-primary" value=" @if ($data['action'] == 'add') Tambah @else Edit @endif">
                </div>

                @if ( isset($data['redirect']) )
                    <a href="{{ route($data['redirect']) }}" class="myredirect pjax"></a>
                @endif

            </form>

            <script>
                mApp.formValidation();
            </script>
        </div>
    </div>
@endsection