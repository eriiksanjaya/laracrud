<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Dashboard@index')->name('dashboard.index');
Route::resource('/siswa', 'Master_siswa');
Route::get('/siswa/{siswa}/delete', 'Master_siswa@delete')->name('siswa.destroy');